package com.example.lv1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val name = findViewById<TextView>(R.id.name)
        val description = findViewById<TextView>(R.id.description)

        val inputName = findViewById<EditText>(R.id.inputName)
        val inputDescription = findViewById<EditText>(R.id.inputDescription)

        val buttonApply = findViewById<Button>(R.id.buttonApply)

        buttonApply.setOnClickListener{
            name.text = inputName.text.toString()
            description.text = inputDescription.text.toString()
        }

        val buttonCalculate = findViewById<Button>(R.id.buttonCalculate)
        val height = findViewById<EditText>(R.id.inputHeight)
        val weight = findViewById<EditText>(R.id.inputWeight)

        buttonCalculate.setOnClickListener{
            val h = height.text.toString().toDouble()
            val w = weight.text.toString().toDouble()

            val bmi : Double = w / (h*h)
            Toast.makeText(this, "%.3f".format(bmi), Toast.LENGTH_LONG).show()
        }
    }
}